FROM node:14-alpine

ENV NODE_ENV=production

WORKDIR /app

ADD ./package.json /app
ADD ./yarn.lock /app

RUN yarn install --prod

ADD . /app

CMD ["yarn", "start"]