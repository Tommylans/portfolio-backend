require('dotenv').config()
import Knex from 'knex';
import fastify from 'fastify'
import {ProjectRepository} from "./repositories/ProjectRepository";
import {ProfileIntroductionRepository} from "./repositories/ProfileIntroductionRepository";


const mysqlConnection = Knex({
    client: 'mysql',
    connection: {
        host: process.env.DATABASE_HOST,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_DATABASE
    }
})


const server = fastify({
    logger: true,
})

const projectRepository = new ProjectRepository(mysqlConnection);
const profileIntroductionRepository = new ProfileIntroductionRepository(mysqlConnection);

server.route({
    method: 'GET',
    url: '/projects',
    handler: async (request, reply) => {
        return await projectRepository.getProjects();
    }
})

server.route({
    method: 'GET',
    url: '/profile-introduction',
    handler: async (request, reply) => {
        return await profileIntroductionRepository.getProfileIntroduction();
    }
})

server.listen(4000, '0.0.0.0')