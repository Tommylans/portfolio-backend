interface IProfileIntroduction {
    id: number
    name: string
    description: string
}

export {IProfileIntroduction};
