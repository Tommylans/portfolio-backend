import {IImage} from "./IImage";

interface IProject {
    id: number
    title: string
    description: string
    logo: string
    images: Array<IImage>
}

export {IProject};
