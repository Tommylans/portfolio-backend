interface IImage {
    url: string
    project_id: number
}

export {IImage};
