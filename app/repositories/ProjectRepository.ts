import Knex from "knex";
import {IProject} from "../models/IProject";
import {IImage} from "../models/IImage";

class ProjectRepository {
    private readonly mysqlConnection: Knex

    constructor(mysqlConnection: Knex) {
        this.mysqlConnection = mysqlConnection;
    }

    async getProjects(): Promise<Array<IProject>> {
        const projects = await this.mysqlConnection
            .select(['id', 'title', 'description', 'logo', 'backgroundColor', 'textColor', 'intro'])
            .from<IProject>('projects');

        const images = await this.mysqlConnection.select(['id', 'url', 'project_id', 'alt', 'height', 'width'])
            .whereIn('project_id', projects.map(project => project.id))
            .from<IImage>('images');

        return projects.map<IProject>((project: IProject) => {
            //TODO Dit moet wat minder dirty en minder hardcoded dat het svg moet zijn.
            project.logo = 'data:image/svg+xml;base64, ' + Buffer.from(project.logo).toString('base64')
            project.images = images.filter(image => image.project_id === project.id);
            return project
        })
    }
}

export {ProjectRepository};