import Knex from "knex";
import {IProfileIntroduction} from "../models/IProfileIntroduction";

class ProfileIntroductionRepository {
    private readonly mysqlConnection: Knex


    constructor(mysqlConnection: Knex) {
        this.mysqlConnection = mysqlConnection;
    }

    async getProfileIntroduction(): Promise<IProfileIntroduction> {
        return this.mysqlConnection
            .select(['id', 'name', 'description'])
            .from<IProfileIntroduction>('profile')
            .first();
    }
}

export {ProfileIntroductionRepository};
